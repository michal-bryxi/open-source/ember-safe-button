import { module, test, skip } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { BASIC_CLASS as BASIC_SAFETY_CLASS } from 'ember-safe-button/components/safety';
import { BASIC_CLASS as BASIC_TRIGGER_CLASS } from 'ember-safe-button/components/trigger';
import { SAFETY_STATUS } from 'ember-safe-button/components/safe-button';
import sinon from 'sinon';

module('Integration | Component | safe-button', function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.set('message', 'foo');
    this.set('safetyMessage', 'bar');
    this.set('triggerMessage', 'baz');
    this.set('confirmSpy', sinon.spy());
    this.set('safetyStatus', SAFETY_STATUS.LOCKED);
  });

  test('basic functionality', async function (assert) {
    await render(
      hbs`<SafeButton @onConfirm={{this.confirmSpy}} @message={{this.message}} />`
    );

    assert.dom(`.${BASIC_SAFETY_CLASS}`).exists().hasText(this.message);
    assert.dom(`.${BASIC_TRIGGER_CLASS}`).exists().hasText(this.message);
  });

  test('block usage', async function (assert) {
    await render(hbs`
      <SafeButton @onConfirm={{this.confirmSpy}} as |button|>
        <button.safety
          data-test-safety
        >
          {{this.safetyMessage}}
        </button.safety>
        <button.trigger
          data-test-trigger
        >
          {{this.triggerMessage}}
        </button.trigger>
      </SafeButton>
    `);

    assert.dom(`[data-test-safety]`).exists().hasText(this.safetyMessage);
    assert.dom(`[data-test-trigger]`).exists().hasText(this.triggerMessage);

    await click(`[data-test-safety]`);

    // TODO: we will need to hook the button animations to ember runloop first
    // await click(`[data-test-trigger]`);
    // assert.ok(this.confirmSpy.calledOnce);
  });

  // TODO: https://github.com/workmanw/ember-qunit-assert-helpers/issues/18
  skip('mandatory arguments', async function (assert) {
    try {
      await render(hbs`<SafeButton />`);
    } catch (error) {
      assert.equal(error.message, 'b');
    }
  });
});
