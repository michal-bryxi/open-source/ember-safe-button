import { module, test } from 'qunit';
import { setupRenderingTest } from 'ember-qunit';
import { click, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { BASIC_CLASS } from 'ember-safe-button/components/trigger';
import { SAFETY_STATUS } from 'ember-safe-button/components/safe-button';
import sinon from 'sinon';

module('Integration | Component | trigger', function (hooks) {
  setupRenderingTest(hooks);

  hooks.beforeEach(function () {
    this.set('message', 'foo');
    this.set('triggerClass', 'bar');
    this.set('confirmSpy', sinon.spy());
    this.set('ariaLabel', 'Confirm action'); // TODO: make this configurable with fallback
    this.set('safetyStatus', SAFETY_STATUS.UNLOCKED);
    this.set('webAnimations', { unlocking: () => {}, locking: () => {} });
  });

  test('basic functionality', async function (assert) {
    await render(
      hbs`<Trigger @safetyStatus={{this.safetyStatus}} @onClick={{this.confirmSpy}} @displayedMessage={{this.message}} class={{this.triggerClass}} />`
    );

    assert
      .dom('button')
      .hasText(this.message)
      .hasAttribute('type', 'button')
      .hasAria('label', this.ariaLabel)
      .hasClass(this.triggerClass)
      .hasClass(BASIC_CLASS);

    await click('button');

    assert.ok(this.confirmSpy.calledOnce);
  });

  test('safety states', async function (assert) {
    await render(
      hbs`<Trigger @onClick={{this.confirmSpy}} @safetyStatus={{this.safetyStatus}} @displayedMessage={{this.message}} @webAnimations={{this.webAnimations}} />`
    );

    assert.dom('button').isNotDisabled().hasAttribute('tabindex', '0');

    this.set('safetyStatus', SAFETY_STATUS.LOCKING);
    assert.dom('button').isDisabled().hasAttribute('tabindex', '-1');

    this.set('safetyStatus', SAFETY_STATUS.LOCKED);
    assert.dom('button').isDisabled().hasAttribute('tabindex', '-1');

    this.set('safetyStatus', SAFETY_STATUS.UNLOCKING);
    assert.dom('button').isDisabled().hasAttribute('tabindex', '-1');
  });

  test('block usage', async function (assert) {
    await render(hbs`<Trigger @onClick={{this.confirmSpy}}>bar</Trigger>`);
    assert.dom('button').hasText('bar');
  });
});
