```js component
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ShowcaseComponent extends Component {
  @action
  safeButtonClicked() {
    alert('Yay!');
  }

  animations = [undefined, 'slide', 'poing', 'lift-bars', 'zoom', 'roll'];
}
```

```hbs template
<h3>
  Rectangle buttons
</h3>

<section class='default-styling-rectangle'>
  {{#each this.animations as |animation|}}
    <SafeButton
      @message={{if animation animation 'default'}}
      @animation={{animation}}
      @onConfirm={{this.safeButtonClicked}}
    />
  {{/each}}
</section>

<h3>
  Circle buttons
</h3>
<section class='default-styling-circle'>
  {{#each this.animations as |animation|}}
    <SafeButton
      @message={{if animation animation 'default'}}
      @animation={{animation}}
      @onConfirm={{this.safeButtonClicked}}
    />
  {{/each}}
</section>
```
