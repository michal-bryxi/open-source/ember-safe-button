---
order: 3
---

# Complex example

- You can add `class` attribute to `trigger`, `safety` and the `safe-button` itself to achieve granular styling.
- It's usually better to put the `trigger` first in the DOM and after that `safety`. It gives more natural flow of `z-index`.
- You can provide `@animation` attribute as an object and then respective parts get passed to given [element.animate()](https://developer.mozilla.org/en-US/docs/Web/API/Element/animate) function.
