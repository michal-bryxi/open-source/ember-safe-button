```js component
import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ComplexExampleButtonComponent extends Component {
  @action
  safeButtonClicked() {
    alert('You deleted Me!');
  }

  animation = {
    safety: {
      unlocking: {
        keyframes: [
          { transform: 'translate3d(0, 0, 0)' },
          { transform: 'translate3d(0, -100%, 0)' },
        ],
        options: {
          duration: 1000,
          fill: 'both',
          easing: 'ease',
        },
      },
      locking: {
        keyframes: [
          { transform: 'translate3d(0, -100%, 0)' },
          { transform: 'translate3d(0, 0, 0)' },
        ],
        options: {
          duration: 1000,
          fill: 'both',
          easing: 'ease',
        },
      },
    },
    trigger: {
      unlocking: {
        keyframes: [],
        options: {
          duration: 1000,
          fill: 'both',
          easing: 'ease',
        },
      },
      locking: {
        keyframes: [],
        options: {
          duration: 1000,
          fill: 'both',
          easing: 'ease',
        },
      },
    },
  };
}
```

```hbs template
    <figure class='md:flex bg-gray-100 rounded-xl p-8 md:p-0'>
      <img
        class='w-32 h-32 md:w-48 md:h-auto md:rounded-none rounded-full mx-auto'
        src='/michal-bryxi.jpg'
        alt='My funny face'
        width='384'
        height='512'
      />
      <div
        class='pt-6 md:p-8 text-center md:text-left space-y-4 flex flex-wrap justify-between'
      >
        <blockquote class='w-full text-lg font-semibold'>
          Cycle 🚴 , climb 🗻 , run 🏃 , travel 🌍 , enjoy life ♥. IT guy with the need to live fully.
        </blockquote>
        <figcaption class='font-medium'>
          <div class='text-cyan-600'>
            Michal Bryxí
          </div>
          <div class='text-gray-500'>
            Fingerpainter
          </div>
        </figcaption>

        <SafeButton
          class='w-16 h-16 block overflow-visible relative'
          @animation={{this.animation}}
          @onConfirm={{this.safeButtonClicked}}
          @timeout={{2000}}
          as |button|
        >
          <button.trigger
            class='rounded-full w-full h-full block absolute left-0 top-0 text-red-600'
          >
            <FaIcon @icon='trash-can' @size='2x' />
          </button.trigger>

          <button.safety
            class='jail w-full h-full block absolute left-0 top-0 bg-contain bg-no-repeat'
          />
        </SafeButton>
      </div>
    </figure>
```
