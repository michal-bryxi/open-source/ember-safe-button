---
order: 1
title: Overview
---

# ember-safe-button

Some actions in your app might be dangerous
and needs a confirmation.
In some situations you might want to use good-old modal dialog
with `ok / cancel` buttons,
but those are usually pretty annoying
and hard to work with
when you need to process bigger number of items.

**ember-safe-button** tries to solve this problem
with a concept of a button
that is covered with a safety
that needs to be deactivated first.

To prevent mishaps over time,
the safety will roll back after a timeout.

## Note

All examples in this documentation
that are wrapped in `class='default-styling-*'`
are going to have some visual styles applied
using [TailwindCSS](https://tailwindcss.com/).
Those are available _only in this demo_.
This won't work in your application.
`ember-safe-button` does **not** have on oppinion
on how you should style your application.
But you are free to quickly copy
the styles from the example `*.css` files
into your Tailwind enabled app and it should just work.

