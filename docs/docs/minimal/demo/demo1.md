```js component
import Component from '@glimmer/component';

export default class MyDemo extends Component {
  safeButtonClicked() {
    alert("Deleted!")
  }
}
```

```hbs template
<SafeButton @message='delete' @onConfirm={{this.safeButtonClicked}} />
```
