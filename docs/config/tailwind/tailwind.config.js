'use strict';

const path = require('path');
const remark = require('remark');

const appRoot = path.join(__dirname, '../../');
const appEntry = path.join(appRoot, 'app');
const relevantFilesGlob = '**/*.{html,js,ts,hbs,gjs,gts,md}';

module.exports = {
  content: {
    files: [path.join(appEntry, relevantFilesGlob)],
    transform: {
      md: (content) => {
        return remark().process(content); // TODO.2: this does nothing
      },
    },
  },
  safelist: [
    {
      pattern: /.*/, // TODO.1: this is a hack to not have styles purged
    },
  ],
  theme: {
    extend: {},
  },
  plugins: [require('@tailwindcss/typography')],
};
