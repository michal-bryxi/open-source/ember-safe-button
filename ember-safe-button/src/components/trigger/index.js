import Component from '@glimmer/component';

import { SAFETY_STATUS } from '../safe-button/index';

export const BASIC_CLASS = 'ember-safe-button-trigger';

/**
  Trigger is a component that is by default covered by safety and triggers the action passed to <SafeButton> when clicked.

  ```hbs
    <SafeButton
      @onConfirm={{action safeButtonClicked}} as |button|
    >
      <button.trigger class="bg-red-dark">
        Trigger
      </button.trigger>
    </SafeButton>
  ```

  @class SafetyComponent
 */
export default class TriggerComponent extends Component {
  BASIC_CLASS = BASIC_CLASS;

  get isDisabled() {
    return this.args.safetyStatus !== SAFETY_STATUS.UNLOCKED;
  }

  get focusMe() {
    return !this.isDisabled;
  }
}
