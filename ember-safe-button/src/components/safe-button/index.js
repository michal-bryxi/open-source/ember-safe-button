import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { assert } from '@ember/debug';
import { action } from '@ember/object';

import { timeout } from 'ember-concurrency';
import { task } from 'ember-concurrency';

import embeddedAnimations from './animations/index';

const DEFAULT_TIMEOUT = 3000;
const DEFAULT_ANIMATION = 'slide';

export const SAFETY_STATUS = {
  LOCKED: 1,
  UNLOCKING: 2,
  UNLOCKED: 3,
  LOCKING: 4,
};

/**
  Main component.

  ```hbs
    <SafeButton
      class="border-black"
      @animation={{this.animation}}
      @message="Delete me"
      @timeout={{2000}}
      @onConfirm={{action safeButtonClicked}} />
  ```

  ```
    @class SafeButtonComponent
    @param {string} [class="ember-safe-button"] CSS class to be applied on the wrapping element.
    @param {object} [animation="slid"] Definition of animation; It can either be string and point to one of the pre-defined animations; Or it can be an object describing Web Animation API
    @param {number} [timeout=3000] Number of millisenconds after which the safety rolls back over the trigger.
    @param {string} [message="delete"] Text to be printed on the buttons in case block is not provided.
    @param {function} onConfirm - Action to trigger whenever user clicks the trigger.
  ```

  When trying to construct your own `@animation` object you can take inspiration from the simple `slide` animation. The thing to keep in mind is that these go directly as parameters to [element.animate()](https://developer.mozilla.org/en-US/docs/Web/API/Element/animate) and that there are in fact four animations of which always two fire at the same time:

  - `safety.unlocking` and `trigger.unlocking`
  - `safety.locking` and `trigger.locking`

  ```js
    {
      safety: {
        unlocking: {
          keyframes: [
            { transform: 'translateX(0)' },
            { transform: 'translateX(-100%)' },
          ],
          options: {
            duration: 1000,
            fill: 'both',
            easing: 'ease',
          },
        },
        locking: {
          keyframes: [
            { transform: 'translateX(-100%)' },
            { transform: 'translateX(0)' },
          ],
          options: {
            duration: 1000,
            fill: 'both',
            easing: 'ease',
          },
        },
      },
      trigger: {
        unlocking: {
          keyframes: [
            { transform: 'translateX(100%)' },
            { transform: 'translateX(0)' },
          ],
          options: {
            duration: 1000,
            fill: 'both',
            easing: 'ease',
          },
        },
        locking: {
          keyframes: [
            { transform: 'translateX(0)' },
            { transform: 'translateX(100%)' },
          ],
          options: {
            duration: 1000,
            fill: 'both',
            easing: 'ease',
          },
        },
      },
    }
  ```
 */
export default class SafeButtonComponent extends Component {
  constructor() {
    super(...arguments);

    assert('@onConfirm attribute must be set', typeof this.args.onConfirm === 'function');
  }

  @tracked safetyStatus = SAFETY_STATUS.LOCKED;

  get activeAnimation() {
    const animation = this.args.animation || DEFAULT_ANIMATION;

    return typeof animation === 'string' ? embeddedAnimations[animation] : animation;
  }

  get webAnimations() {
    const { activeAnimation, onUnlocked, onLocked } = this;

    return {
      safety: {
        unlocking: ({ element }) => {
          let animation = element.animate(
            activeAnimation.safety.unlocking.keyframes,
            activeAnimation.safety.unlocking.options
          );
          animation.onfinish = onUnlocked;
        },
        locking: ({ element }) => {
          let animation = element.animate(
            activeAnimation.safety.locking.keyframes,
            activeAnimation.safety.locking.options
          );
          animation.onfinish = onLocked;
        },
      },
      trigger: {
        unlocking({ element }) {
          element.animate(
            activeAnimation.trigger.unlocking.keyframes,
            activeAnimation.trigger.unlocking.options
          );
        },
        locking({ element }) {
          element.animate(
            activeAnimation.trigger.locking.keyframes,
            activeAnimation.trigger.locking.options
          );
        },
      },
    };
  }

  get timeout() {
    return this.args.timeout || DEFAULT_TIMEOUT;
  }

  get displayedMessage() {
    return this.args.message || 'delete';
  }

  @action
  onUnlocking() {
    this.safetyStatus = SAFETY_STATUS.UNLOCKING;
  }

  @action
  onUnlocked() {
    this.safetyStatus = SAFETY_STATUS.UNLOCKED;
    this.triggerSafety.perform();
  }

  @action
  onLocked() {
    this.safetyStatus = SAFETY_STATUS.LOCKED;
  }

  @task(function* () {
    yield timeout(this.timeout);
    this.safetyStatus = SAFETY_STATUS.LOCKING;
  })
  triggerSafety;
}
