export default {
  safety: {
    unlocking: {
      keyframes: [
        { transform: 'translateX(0)' },
        { transform: 'translateX(-100%) rotate(-180deg)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
    locking: {
      keyframes: [
        { transform: 'translateX(-100%) rotate(-180deg)' },
        { transform: 'translateX(0)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
  },
  trigger: {
    unlocking: {
      keyframes: [
        { transform: 'translateX(100%) rotate(180deg)' },
        { transform: 'translateX(0)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
    locking: {
      keyframes: [
        { transform: 'translateX(0)' },
        { transform: 'translateX(100%) rotate(180deg)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
  },
};
