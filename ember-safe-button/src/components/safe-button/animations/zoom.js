export default {
  safety: {
    unlocking: {
      keyframes: [
        { offset: 0, transform: 'translate3d(0, 0, 0)' },
        { offset: 1, transform: 'translate3d(100%, 0, 0)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
    locking: {
      keyframes: [
        { offset: 0, transform: 'translate3d(-100%, 0, 0)' },
        { offset: 1, transform: 'translate3d(0, 0, 0)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
  },
  trigger: {
    unlocking: {
      keyframes: [
        { offset: 0, opacity: 0, transform: 'scale3d(0.3, 0.3, 0.3)' },
        { offset: 0.5, opacity: 1 },
        { offset: 1, opacity: 1 },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
    locking: {
      keyframes: [
        { offset: 0, opacity: 1 },
        { offset: 0.5, opacity: 0, transform: 'scale3d(0.3, 0.3, 0.3)' },
        { offset: 1, opacity: 0 },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
  },
};
