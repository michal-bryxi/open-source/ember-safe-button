export default {
  safety: {
    unlocking: {
      keyframes: [
        { transform: 'translate3d(0, 0, 0)' },
        { transform: 'translate3d(0, -100%, 0)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
    locking: {
      keyframes: [
        { transform: 'translate3d(0, -100%, 0)' },
        { transform: 'translate3d(0, 0, 0)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
  },
  trigger: {
    unlocking: {
      keyframes: [
        // { transform: 'translateX(100%)' },
        // { transform: 'translateX(0)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
    locking: {
      keyframes: [
        // { transform: 'translateX(0)' },
        // { transform: 'translateX(100%)' },
      ],
      options: {
        duration: 1000,
        fill: 'both',
        easing: 'ease',
      },
    },
  },
};
