import { modifier } from 'ember-modifier';

import { SAFETY_STATUS } from '../components/safe-button/index';

export default modifier(
  (element, _, named) => {
    if (named.focusMe) {
      element.focus();
    }

    if (named.safetyStatus === SAFETY_STATUS.UNLOCKING) {
      named.webAnimations.unlocking({
        element,
      });
    } else if (named.safetyStatus === SAFETY_STATUS.LOCKING) {
      named.webAnimations.locking({
        element,
      });
    }
  },
  { eager: false }
);
