![ember-safe-button logo](/docs/public/logo.png) 

# ember-safe-button

Provides a button for potentially dangerous actions that better needs a confirmation. First click unlocks the fuse, second then confirms. If no confirmation is provided, then the button returns the safety in place after a timeout.


Compatibility
------------------------------------------------------------------------------

* Ember.js v3.20 or above
* Ember CLI v3.20 or above
* Node.js v12 or above


## Installation

```
ember install ember-safe-button
```


## Usage

Minimal example:

```hbs
<SafeButton
  @message="delete"
  @onConfirm={{this.safeButtonClicked}}
/>
```

Which will produce:

![Example](/docs/public/demo.gif)

## Documentation

For many more examples and API design see [ember-safe-button documentation website](https://ember-safe-button.netlify.com).

## Contributing

See the [Contributing](CONTRIBUTING.md) guide for details.

## Compatibility

* Ember.js v3.16 or above
* Ember CLI v2.13 or above
* Node.js v10 or above

## Contributors

- [Michal Bryxí](https://twitter.com/MichalBryxi)
- [Karel Funda](https://twitter.com/fundix2)

## License

This project is licensed under the [MIT License](LICENSE.md).

## Attribution

<div>Icons made by <a href="https://www.flaticon.com/authors/dinosoftlabs" title="DinosoftLabs">DinosoftLabs</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
<div>Icons made by <a href="https://www.flaticon.com/authors/surang" title="surang">surang</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
